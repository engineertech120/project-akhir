@extends('layouts.master')

@section('title')
Halaman List Game
@endsection

@section('content')
<form method="POST" action="/game">
    @csrf
    <div class="form-group">
      <label>Game Name</label>
      <input type="text" class="form-control" name="name" aria-describedby="emailHelp">
    </div>

    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Gameplay</label>
      <textarea name="gameplay" class="form-control" id="" cools="30" rows="10"></textarea>
    </div>
    @error('gameplay')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Developer Game</label>
        <input type="text" class="form-control" name="developer" aria-describedby="emailHelp">
      </div>
      @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun" aria-describedby="emailHelp">
      </div>
      @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
